import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';


@Component({
  selector: 'recipe-list',
  templateUrl: './recipelist.component.html',
  styleUrls: ['./recipelist.component.css']
})

export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('Tater Tot Hot Dish', 'The favorite of the upper-midwest', 'https://cook.fnr.sndimg.com/content/dam/images/cook/fullset/2012/6/15/0/CC_Hot-Dish-Tater-Tot-Casserole-Recipe_s4x3.jpg.rend.hgtvcom.826.620.suffix/1358449564114.jpeg')
  ];

  constructor() { }

  ngOnInit() { }
}
