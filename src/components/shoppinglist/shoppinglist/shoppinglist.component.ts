import { Component, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model'

@Component({
  selector: 'shopping-list',
  templateUrl: './shoppinglist.component.html',
  styleUrls: ['./shoppinglist.component.css']
})

export class ShoppingListComponent implements OnInit {
  ingredients: Ingredient[] = [
    new Ingredient('Bag of Tater Tots', 1),
    new Ingredient('Can of Cream of Mushroom', 2)
  ];

  constructor() { }

  ngOnInit() { }
}
