import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent} from './header/header.component';
import { RecipeDetailComponent } from 'src/components/recipebook/recipedetail/recipedetail.component';
import { RecipeItemComponent } from '../components/recipebook/recipeitem/recipeitem.component';
import { RecipeListComponent } from '../components/recipebook/recipelist/recipelist.component';
import { ShoppingListComponent } from '../components/shoppinglist/shoppinglist/shoppinglist.component';
import { ShoppingListEditorComponent } from '../components/shoppinglist/shoppinglistedit/shoppinglistedit.component';
import { RecipesComponent } from '../components/recipebook/recipes/recipes.component'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    RecipeListComponent,
    ShoppingListComponent,
    ShoppingListEditorComponent,
    RecipesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
